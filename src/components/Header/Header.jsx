import React, { useEffect, useState } from "react";
import style from "./header.module.css";
import PinterestIcon from "../Icon/Icon";

const Header = () => {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.pageYOffset;
      if (scrollTop > 0) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <header className={`${style.container}${isScrolled ? style.shadow : null}`}>
      <div className="h-full">
        <PinterestIcon className="" />
      </div>
    </header>
  );
};

export default Header;
